FROM alpine:latest AS grab
# FROM python:slim
# RUN apt-get update && apt-get install -y curl && apt-get autoremove --purge -y && apt-get -y clean && rm -rf /var/lib/apt/lists/*

RUN apk add curl openssl
RUN curl -L "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_Linux_amd64.tar.gz" | tar xz -C /usr/local/bin
RUN curl -o /tmp/get-helm-3 -L https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && ash /tmp/get-helm-3
RUN curl -o /usr/local/bin/kubectl -L https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/kubectl && chmod +x /usr/local/bin/kubectl
RUN curl -o /usr/local/bin/aws-iam-authenticator -L https://amazon-eks.s3.us-west-2.amazonaws.com/1.15.10/2020-02-22/bin/linux/amd64/aws-iam-authenticator && chmod +x /usr/local/bin/aws-iam-authenticator
# RUN curl -sL https://run.linkerd.io/install | sh && ln -s ~/.linkerd2/bin/linkerd /usr/local/bin/linkerd

FROM python:slim
RUN pip install --quiet aws awscli
COPY --from=grab /usr/local/bin/* /usr/local/bin